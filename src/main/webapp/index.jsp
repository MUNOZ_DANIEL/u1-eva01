<%-- 
    Document   : index
    Created on : Apr 12, 2021, 1:24:59 AM
    Author     : danielmunoz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            form {
                /* Centrar el formulario en la página */
                margin: 0 auto;
                width: 400px;
                /* Esquema del formulario */
                padding: 1em;
                border: 1px solid #CCC;
                border-radius: 1em;
            }
        </style>
        <title>Calculadora I.C. Page</title>
    </head>
    <body>
        <header>

        </header>
        <section>
            <div style="text-align: center">
                <h3 style="color:blue">Calculadora de Interés Simple con Periodos Anuales</h3> 
            </div>
            <form name="form" action="controlador" method="POST">
                <table style="width:400px; margin: auto" >
                    <tr>
                        <th style="text-align: center">
                            <h5>DATOS PARA EL CALCULO</h5>
                        </th>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <div class="formgroup">
                                <label>Monto del Capital    $ </label>
                                <input 
                                    type="text" 
                                    name="capital" 
                                    placeholder="123456789"
                                    style="width:150px; height:20px" 
                                    size=20 
                                    required
                                    autofocus />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="formgroup">
                                <label>Tasa de interés Anual </label>
                                <input 
                                    type="text" 
                                    name="tasaInteresAnual" 
                                    placeholder="100"
                                    style="width:150px; height:20px"                
                                    required
                                    autofocus />%
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="formgroup">
                                <label>Años </label>
                                <input 
                                    type="text" 
                                    name="years" 
                                    placeholder="10"
                                    style="width:150px; height:20px"
                                    required
                                    autofocus />
                            </div>
                        </td>
                    </tr>
                </table>
                <center>
                    <br />
                    <input type="submit" value="Enviar" />
                </center>
            </form>
        </section>
        <!-- INFORMACION ALUMNO -->
        <footer>
            <div>
                <br />
                <center>
                    <p>Asignatura         : Taller de Aplicaciones Empresariales - IC201IECIREOL</p>
                    <p>Nombre del Alumno  : DANIEL MUÑOZ PASTENE</p>
                    <p>Desarrollo Ver.1.0 : April 2021</p>
                    <p>Sección            : 51</p>
                    <p>Entrega            : Evaluación Unidad 1</p>
                    <img src="LOGO_CIISA_apaisado_PNG.png" width="120" height="70" alt="LOGO_CIISA_apaisado_PNG"/>
                </center>
            </div>
        </footer>
        <!-- INFORMACION ALUMNO -->
    </body>
</html>
