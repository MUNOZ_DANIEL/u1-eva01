<%-- 
    Document   : resultado
    Created on : Apr 12, 2021, 2:38:11 AM
    Author     : danielmunoz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado Cálculo</title>
        <style>
            form {
                /* Centrar el formulario en la página */
                margin: 0 auto;
                width: 600px;
                /* Esquema del formulario */
                padding: 1em;
                border: 1px solid #CCC;
                border-radius: 1em;
                background:cornsilk;
            }
        </style>
    </head>
    <body>
        <%
            String capital = (String) request.getAttribute("capital");
            String tasaInteresAnual = (String) request.getAttribute("tasaInteresAnual");
            String years = (String) request.getAttribute("years");
            String ganancia = (String) request.getAttribute("ganancia");

        %>
        <section>
            <form name="FormResultado" method="">
                <div>
                    <center>
                   
                        <h3>Su capital de inversión es <b style="color:blue">$<%=capital%></b> pesos<br />
                            a una tasa anual del <b style="color:blue"><%=tasaInteresAnual%>%</b><br />
                            en <b style="color:blue"><%=years%></b> años<br />
                            genera un interés simple de <b style="color:blue">$<%=ganancia%></b> pesos</h3>
                    </center>
                </div>
            </form>
        </section>
        <!-- INFORMACION ALUMNO -->
        <footer>
            <div>
                <br />
                <center>
                    <p>Asignatura         : Taller de Aplicaciones Empresariales - IC201IECIREOL</p>
                    <p>Nombre del Alumno  : DANIEL MUÑOZ PASTENE</p>
                    <p>Desarrollo Ver.1.0 : April 2021</p>
                    <p>Sección            : 51</p>
                    <p>Entrega            : Evaluación Unidad 1</p>
                    <img src="LOGO_CIISA_apaisado_PNG.png" width="120" height="70" alt="LOGO_CIISA_apaisado_PNG"/>
                </center>
            </div>
        </footer>
        <!-- INFORMACION ALUMNO -->
    </body>
</html>
