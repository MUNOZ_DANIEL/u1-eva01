package Test.Packages;


import java.util.Scanner;
import root.modelo.InteresCompuesto;



/**
 *
 * @author danielmunoz
 */
public class TestModelo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        InteresCompuesto pruebaCalculo = new InteresCompuesto();
        
        float capital=0;
        float interes=0;
        float annios=0;
//        String resultado;        
        Scanner teclado = new Scanner(System.in);
        
        System.out.println("valor capital : ");
        capital=teclado.nextFloat();
        System.out.println("valor interes : ");
        interes=teclado.nextFloat();
        System.out.println("valor años : ");
        annios=teclado.nextFloat();
        pruebaCalculo.getTestCalculoInteres(capital, interes, annios);
        
    }
    
}
