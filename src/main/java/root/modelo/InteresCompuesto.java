
package root.modelo;

import java.text.DecimalFormat;

/**
 *
 * @author danielmunoz
 */
public class InteresCompuesto {
    
    private float capital;
    private float tasaInteresAnual;
    private float yearsCredito;
    
//    DecimalFormat resultado = new DecimalFormat("#.00");

    /**
     * @return the capital
     */
    public float getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the tasaInteresAnual
     */
    public double getTasaInteresAnual() {
        return tasaInteresAnual;
    }

    /**
     * @param tasaInteresAnual the tasaInteresAnual to set
     */
    public void setTasaInteresAnual(float tasaInteresAnual) {
        this.tasaInteresAnual = tasaInteresAnual;
    }

    /**
     * @return the yearsCredito
     */
    public float getYearsCredito() {
        return yearsCredito;
    }

    /**
     * @param yearsCredito the yearsCredito to set
     */
    public void setYearsCredito(int yearsCredito) {
        this.yearsCredito = yearsCredito;
    }
    
    public String getCalculoInteres(float c, float i, float n){ 
        /*
            I = C * (i/100) * n; 
            donde I = interés simple producido,
            C = capital, 
            i = tasa interés anual y 
            n = número de años 
        */
        double factor= (i/100);
        String toCadena;
        
        DecimalFormat resultado = new DecimalFormat("#.00");

        float calculaGanancia = (float)(c*factor*n);
        /*
            FARMATO A DOS DECIMALES CON PASO A STRING
        */
        toCadena = (resultado.format(calculaGanancia));
        
        return toCadena;

    }
    
    public void getTestCalculoInteres(float c, float i, float n){ 
        /*
            I = C * (i/100) * n; 
            donde I = interés simple producido,
            C = capital, 
            i = tasa interés anual y 
            n = número de años 
        */
        double factor= (i/100);
        String toCadena;
        
        DecimalFormat resultado = new DecimalFormat("#.00");

        float calculaGanancia = (float)(c*factor*n);
        /*
            FARMATO A DOS DECIMALES CON PASO A STRING
        */
        toCadena = (resultado.format(calculaGanancia));
        
        System.out.println("resultado "+ toCadena);
    }
    
}

